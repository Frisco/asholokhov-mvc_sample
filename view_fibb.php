<?php

abstract class ViewBase {
    abstract public function defeaultView();
}

class ViewFibbonaci extends ViewBase {
    
    public function defeaultView() {
	$html = "<form method='GET'>
		    <input type='text' name='fibb_count' />
		    <input type='submit' value='Generate!' />
		 </form>";
	
	return $html;
    }
    
    public function tableView($data) {
	if (!isset($data)) {
	    $html = "Error: data is null.";
	    return $html;
	}
	
	$html = "<table align='center' border='1px'>
		    <tr>
			<td>#</td>
			<td>Member</td>
		    </tr>";
	
	foreach ($data as $key => $num) {
	    $html.= "<tr>\n";
	    $html.= " <td>{$key}</td>\n";
	    $html.= " <td>{$num}</td>\n";
	    $html.= "</tr>\n";
	}
	$html.= "</table>";
	    
	return $html;
    }
    
}

?>
