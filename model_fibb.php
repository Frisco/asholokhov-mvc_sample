<?php

abstract class ModelBase {
    protected $rawData;
    
    public function __construct($source_data) {
	$this->rawData = $source_data;
    }

    abstract public function calculate();
    abstract public function setRawData($data);
    abstract public function getCalculatedData();
    
    function getRawData() {
	return $this->rawData;
    }
}

class ModelFibbonaci extends ModelBase {
    private $finalData;
    
    public function __construct($source_data) {
	parent::__construct($source_data);
	$this->finalData = null;
    }

    public function calculate() {
	$result = array();
	
	$result[0] = 1;
	$result[1] = 1;
	
	for ($i = 2; $i < $this->rawData; $i++) {
	    $result[$i] = $result[$i - 1] + $result[$i - 2];
	}
	
	$this->finalData = $result;
    }
    
    public function setRawData($rawData) {
	$this->rawData = $rawData;
    }

    public function getCalculatedData() {
	if (isset($this->finalData)) 
	    return $this->finalData;
	return null;
    }
    
}


?>
